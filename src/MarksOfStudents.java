import java.util.*;

public class MarksOfStudents {
    private final HashMap<Integer, String> nameOfStudent = new HashMap<>();
    private final HashMap<Integer, ArrayList<Integer>> marksOfStudent = new HashMap<>();

    MarksOfStudents(Integer rollNo, String name) {
        this.nameOfStudent.put(rollNo, name);
    }

    public void setMarksOfStudent(Integer roll, Integer marksInMath, Integer marksInPhysics, Integer marksInChemistry, Integer marksInCS, Integer marksInBiology){
        ArrayList<Integer> marks = new ArrayList<>();
        marks.add(marksInPhysics);
        marks.add(marksInChemistry);
        marks.add(marksInMath);
        marks.add(marksInCS);
        marks.add(marksInBiology);
        this.marksOfStudent.put(roll, marks);
    }


    public void printInformationOfAllStudents(){
        for(Map.Entry<Integer, String> entry : this.nameOfStudent.entrySet()){
            Integer rollNo = entry.getKey();
            String name = entry.getValue();

            System.out.println(rollNo + ": " + name);
            System.out.println(name + "'s marks: " + this.marksOfStudent.get(rollNo));
        }
    }

    public void addStudentInDatabase(Integer roll, String name){
        this.nameOfStudent.put(roll, name);
    }

    public void classifyStudent(){
        ArrayList<String> studentsInCSE = new ArrayList<>();
        ArrayList<String> studentsInBiology = new ArrayList<>();
        ArrayList<String> studentsInCommerce = new ArrayList<>();

        for(Map.Entry<Integer, String> entry : this.nameOfStudent.entrySet()) {
            Integer rollNo = entry.getKey();
            String name = entry.getValue();

            ArrayList<Integer> marks = this.marksOfStudent.get(rollNo);
            double marksInPCM = 0;

            for(int i = 0; i < 3; i++){
                marksInPCM += (double)marks.get(i);
            }

            marksInPCM = marksInPCM / 3;

            if(marksInPCM > 70 && marks.get(3) > 80){
                studentsInCSE.add(name);
            }
            else if(marksInPCM > 70){
                studentsInBiology.add(name);
            }
            else if(marks.get(2) > 80){
                studentsInCommerce.add(name);
            }
        }

        System.out.print("Students in CSE: ");
        for(String name: studentsInCSE){
            System.out.print(name + ", ");
        }

        System.out.println();

        System.out.print("Students in Biology: ");
        for(String name: studentsInBiology){
            System.out.print(name + ", ");
        }

        System.out.println();

        System.out.print("Students in Commerce: ");
        for(String name: studentsInCommerce){
            System.out.print(name + ", ");
        }

        System.out.println();
    }

    public void rankStudents(){
        TreeMap<Integer, Integer> totalMarks = new TreeMap<>();


        for(Map.Entry<Integer, String> entry : this.nameOfStudent.entrySet()) {
            Integer rollNo = entry.getKey();
            int totalMarksInAllSubjects = 0;

            ArrayList<Integer> marks = this.marksOfStudent.get(rollNo);

            for (Integer marksElement : marks) {
                totalMarksInAllSubjects += marksElement;
            }

            totalMarks.put(totalMarksInAllSubjects, rollNo);
        }

        for(Map.Entry<Integer, Integer> entry : totalMarks.entrySet()){
            System.out.println(this.nameOfStudent.get(entry.getValue()) + ": " + entry.getKey());
        }

    }


    public static void main(String[] args){
        MarksOfStudents marksOfStudents = new MarksOfStudents(1, "Isha Singh");
        marksOfStudents.setMarksOfStudent(1, 90, 90, 90, 90, 90);
        marksOfStudents.addStudentInDatabase(2, "Anupam Anurag");
        marksOfStudents.setMarksOfStudent(2, 81, 80, 80, 80, 80);
        marksOfStudents.addStudentInDatabase(3, "Siya Singh");
        marksOfStudents.setMarksOfStudent(3, 85,77, 60, 95, 77);
        marksOfStudents.addStudentInDatabase(4, "Saharsh Vaibhav");
        marksOfStudents.setMarksOfStudent(4, 88, 85, 55, 45, 61);
        marksOfStudents.printInformationOfAllStudents();
        System.out.println();
        marksOfStudents.classifyStudent();
        System.out.println();
        System.out.println("The order of students in increasing order of marks is-> ");
        marksOfStudents.rankStudents();
    }

}
